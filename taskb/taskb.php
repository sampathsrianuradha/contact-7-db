<?php
/*
Plugin Name: Task B
Plugin URI: http://localhost/plugin
Description: Contact form 7 data store plugin
Version: 1.0.0
Author: Sampath Sri Anuradha 
License: GPLv2
*/

defined( 'ABSPATH' ) or die( 'Hey, you can not access this file.' );

if ( ! class_exists( 'Messages' ) ) {
   require_once( 'messages.php' );
}

class Task_Plugin
{
   static $instance;
   public $messages;

   public function __construct()
   {
      add_filter('set-screen-option', array(__CLASS__, 'set_screen'), 10, 3);
      add_action('admin_menu', array($this, 'plugin_menu'));
      add_action( 'wpcf7_before_send_mail', array( $this,'save_before_send_mail') );
   }

   public static function plugin_activate() {
      global $wpdb;
      $table = $wpdb->prefix . 'cf7database';
      if ($wpdb->get_var("SHOW TABLES LIKE '$table'") != $table) {
         $charset = $wpdb->get_charset_collate();
         $sql = "CREATE TABLE $table (
                     id mediumint(9) NOT NULL AUTO_INCREMENT,
                     fullname tinytext NOT NULL,
                     contact_number mediumint(8) NOT NULL,
                     message text NOT NULL,
                     submiteddatetime timestamp,
                     PRIMARY KEY (id)
            )    $charset;";

         require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
         dbDelta($sql);
      }
   }

   function save_before_send_mail()
   {
      global $wpdb;
      $table = "{$wpdb->prefix}cf7database";

      $form_to_DB = WPCF7_Submission::get_instance();
      if ($form_to_DB) {
         $posted = $form_to_DB->get_posted_data();
         $fullname = $posted['full-name'];
         $contact_number = $posted['contact-number'];
         $message = $posted['message'];
      }

      $wpdb->insert( $table, array( 'fullname' =>$fullname , 'contact_number'=>$contact_number, 'message'=> $message ), array( '%s' ) );
   }


   public static function set_screen($status, $option, $value)
   {
      return $value;
   }

   public function plugin_menu()
   {

      $hook = add_menu_page(
         'Contact 7 DB',
         'CF 7 DB',
         'manage_options',
         'contact_messages',
         [$this, 'plugin_settings_page']
      );

      add_action("load-$hook", [$this, 'screen_option']);

   }

   public function screen_option()
   {

      $option = 'per_page';
      $args = [
         'label' => 'Contact 7 DB',
         'default' => 5,
         'option' => 'messages_per_page'
      ];
      add_screen_option($option, $args);
      //Messages obj
      $this->messages = new Messages();
   }

   public function plugin_settings_page()
   {
      ?>
      <div class="wrap">
         <h2>Contact messages</h2>

         <div id="messages">
            <div id="post-body" class="metabox-holder columns-2">
               <div id="post-body-content">
                  <div class="meta-box-sortables ui-sortable">
                     <form method="post">
                        <?php
                        $this->messages->prepare_items();
                        $this->messages->display(); ?>
                     </form>
                  </div>
               </div>
            </div>
            <br class="clear">
         </div>
      </div>
      <?php
   }

   public static function get_instance()
   {
      if (!isset(self::$instance)) {
         self::$instance = new self();
      }

      return self::$instance;
   }

}
add_action( 'plugins_loaded', function () {
   Task_Plugin::get_instance();
} );
register_activation_hook( __FILE__, array( 'Task_Plugin', 'plugin_activate' ) );
