<?php
/**
 * Format and retrieve data from contact 7 DB
 *
 */

if ( ! class_exists( 'WP_List_Table' ) ) {
   require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class Messages extends WP_List_Table
{

   public function __construct()
   {
      parent::__construct([
         'singular' => __('Message', 'task'),
         'plural' => __('Messages', 'task'),
      ]);

   }

   public static function get_messages($per_page = 5, $page_number = 1)
   {

      global $wpdb;

      $sql = "SELECT * FROM {$wpdb->prefix}cf7database";
      if (!empty($_REQUEST['orderby'])) {
         $sql .= ' ORDER BY ' . esc_sql($_REQUEST['orderby']);
         $sql .= !empty($_REQUEST['order']) ? ' ' . esc_sql($_REQUEST['order']) : ' ASC';
      }

      $sql .= " LIMIT $per_page";
      $sql .= ' OFFSET ' . ($page_number - 1) * $per_page;
      $result = $wpdb->get_results($sql, 'ARRAY_A');

      return $result;
   }

   public static function delete_message($id)
   {
      global $wpdb;

      $wpdb->delete(
         "{$wpdb->prefix}cf7database",
         ['id' => $id],
         ['%d']
      );
   }


   public static function record_count()
   {
      global $wpdb;

      $sql = "SELECT COUNT(*) FROM {$wpdb->prefix}cf7database";
      return $wpdb->get_var($sql);
   }

   public function no_items()
   {
      _e('No customer contact messages avaliable.', 'task');
   }

   public function column_name($item)
   {
      $delete_nonce = wp_create_nonce('task_delete_message');
      $title = '<strong>' . $item['fullname'] . '</strong>';
      $actions = [
         'delete' => sprintf('<a href="?page=%s&action=%s&message=%s&_wpnonce=%s">Delete</a>', esc_attr($_REQUEST['page']), 'delete', absint($item['ID']), $delete_nonce)
      ];

      return $title . $this->row_actions($actions);
   }

   public function column_default($item, $column_name)
   {
      switch ($column_name) {
         case 'fullname':
         case 'contact_number':
         case 'message':
         case 'submiteddatetime':
            return $item[$column_name];
         default:
            return print_r($item, true); //Show the whole array for troubleshooting purposes
      }
   }

   function column_cb($item)
   {
      return sprintf(
         '<input type="checkbox" name="bulk-delete[]" value="%s" />', $item['id']
      );
   }

   function get_columns()
   {
      $columns = [
         'cb' => '<input type="checkbox" />',
         'fullname' => __('Full Name', 'task'),
         'contact_number' => __('Contact number', 'task'),
         'message' => __('Message', 'task'),
         'submiteddatetime' => __('Created', 'task')
      ];

      return $columns;
   }


   public function get_sortable_columns()
   {
      $sortable_columns = array(
         'fullname' => array('fullname', true),
         'contact_number' => array('contact_number', false)
      );

      return $sortable_columns;
   }

   public function get_bulk_actions()
   {
      $actions = [
         'bulk-delete' => 'Delete'
      ];

      return $actions;
   }

   public function prepare_items()
   {
      $this->_column_headers = $this->get_column_info();
      $this->process_bulk_action();

      $per_page = $this->get_items_per_page('messages_per_page', 5);
      $current_page = $this->get_pagenum();
      $total_items = self::record_count();

      $this->set_pagination_args([
         'total_items' => $total_items,
         'per_page' => $per_page
      ]);

      $this->items = self::get_messages($per_page, $current_page);
   }

   public function process_bulk_action()
   {
      if ('delete' === $this->current_action()) {

         $nonce = esc_attr($_REQUEST['_wpnonce']);
         if (!wp_verify_nonce($nonce, 'task_delete_message')) {
            die('killed');
         } else {
            self::delete_message(absint($_GET['message']));
            wp_redirect(esc_url(add_query_arg()));
            exit;
         }

      }
      if ((isset($_POST['action']) && $_POST['action'] == 'bulk-delete')
         || (isset($_POST['action2']) && $_POST['action2'] == 'bulk-delete')
      ) {

         $delete_ids = esc_sql($_POST['bulk-delete']);

         foreach ($delete_ids as $id) {
            self::delete_message($id);

         }

         wp_redirect(esc_url(add_query_arg()));
         exit;
      }
   }
}