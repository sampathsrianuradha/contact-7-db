<?php
class TaskbActivate
{
   public function activate(){
      self::create_table();
   }
   
   public function create_table(){
      global $wpdb;
      $table_name = $wpdb->prefix . "_cf7database";
      $charset_collate = $wpdb->get_charset_collate();

      if ( $wpdb->get_var( "SHOW TABLES LIKE '{$table_name}'" ) != $table_name ) {

         $sql = "CREATE TABLE $table_name (
                  'ID' mediumint(9) NOT NULL AUTO_INCREMENT,
                  'fullname' text NOT NULL,
                  'contact_number' text NOT NULL,
                  'message' text NOT NULL,
                  'submiteddatetime' timestamp
                  PRIMARY KEY  (ID)
         )    $charset_collate;";

         require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
         dbDelta( $sql );
      }
   }
}
